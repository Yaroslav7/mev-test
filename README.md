Test task
========================

Requirements:
---------------
* Check [symfony3](http://symfony.com/doc/current/reference/requirements.html) requirements
* Will need [git](https://git-scm.com/download) to get project

Installation:
--------------

1.Download from git: 
```console
    git clone git@bitbucket.org:Yaroslav7/mev-test.git
```
2.Install libs: 
```console
    composer update
    bower update
```
3.Install assets in the web folder, if there is no them:
```console
    php bin/console assets:install web
```
Configuration
-------------

Set folder for saved attachments:
```yml
#config.yml
parameters:
    upload_directory: '%kernel.root_dir%/../web/uploads/files' #change current route
```

Doctrine ORM
-------------
To create the database launch the following commands:
```console
    php bin/console doctrine:database:create
    php bin/console doctrine:schema:update --force
```


What's inside?
--------------
1. [Symfony3](https://github.com/symfony/symfony-standard) default bundles
2. [FOSUserBundle](https://github.com/FriendsOfSymfony/FOSUserBundle) - used for users registration and login
3. [PHPUnit](https://github.com/sebastianbergmann/phpunit) - used for tests
4. [Bootstrap](https://github.com/twbs/bootstrap) - used for frontend styles
5. [jQuery](https://github.com/jquery/jquery) and [jQuery-ui](https://github.com/jquery/jquery-ui) - used for interface interactions and effects
6. [Google APIs from javascript](https://apis.google.com/js/client:platform.js) - used for google account authentication
 
ER-model of current database
-------------------------------

 ![Conceptual model](https://pp.vk.me/c637820/v637820856/20fe/4q1BXl2kEwU.jpg 'Conceptual model')
