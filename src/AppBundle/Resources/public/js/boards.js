var Board = new function () {

    /**
     * Getting form for creating of new board
     */
    this.getNewBoardForm = function () {
        $.post("/getnewboardform", function (data) {
            $('#add_board').html(data);
            $('#add_board').show();
        });
    };

    /**
     * Getting buttin for getting form for creating of new board
     */
    this.cancelNewBoard = function () {
        $('#add_board').html('');
        $('#add_board').hide();
    };

    /**
     * Sent request for creation of new board
     */
    this.addNewBoard = function () {
        var name = $('#new_board_name').val().trim();
        if (name == '') {
            alert("Please, set board name");
        }
        else {
            $.post("/addnewboardform", {data: name}, function (data) {
                Board.updateBoard();
            });
        }
    };
    /**
     * Getting current boards
     */
    this.updateBoard = function () {
        $.post("/getupdatedboard", function (data) {
            $('.boards').html(data);
            $('.black-background').hide();
            $('#fountainG').hide();
            $('.columns-page').remove();
            $('body').removeClass('board_opened');
        });
    };

    /**
     * Getting form for creating new cart
     *
     * @param column_id
     */
    this.getNewCartForm = function (column_id) {
        $.post("/getnewcartform", {column_id: column_id}, function (data) {
            $('.carts').html(data);
            window.scrollTo(15, 0);
            $('.black-background').show();
            $('.carts').show();
        });
    };
    /**
     * Getting information about chosen cart
     *
     * @param cart_id
     */
    this.openCart = function (cart_id) {
        $.post("/getcartinfo", {cart_id: cart_id}, function (data) {
            if (data == 'false') {
                alert('Something wrong with cart info. You cannot open it.');
            }
            else {
                $('.carts').html(data);
                window.scrollTo(15, 0);
                $('.black-background').show();
                $('.carts').show();
            }
        });
    };

    /**
     * Getting form for creation new column
     *
     * @param board_id
     * @param btn
     */
    this.getNewColum = function (board_id, btn) {
        $(btn).parents().find('.add_new_column').remove();
        $.post("/getnewcolumnform", {board_id: board_id}, function (data) {
            $(btn).before(data);
        });
    };

    /**
     * Sent request for adding new column
     *
     * @param board_id
     */
    this.addNewColumn = function (board_id) {
        var name = $('#new_column_name').val().trim();
        if (name == '') {
            alert("Please, set column name");
        }
        else {
            $.post("/addcolumn", {board_id: board_id, name: name}, function (data) {
                Board.openBoard(board_id);
            });
        }
    };

    /**
     * Removing new column form
     *
     * @param btn
     */
    this.cancelNewColumn = function (btn) {
        $(btn).parent().parent().find('.add_new_column').remove();
    };

    /**
     * Removing form of adding new cart
     *
     * @param event
     */
    this.cancelNewCart = function (event) {
        event.preventDefault();
        $('.carts').hide();
        $('.carts').html('');
        $('.black-background').hide();
    };


    /**
     * Getting form for editing chosen column name
     *
     * @param column
     */
    this.editColumn = function (column) {
        $(column).parent().parent().find('.custom-column-title').append('<p class="editing_column"> ' +
            '  <input id="new_column_name" class="form-control" placeholder="Enter new name"/>' +
            '  <button class="btn add-board-btn" onclick="Board.updateColumnName(this)">' +
            '    Update' +
            '  </button> ' +
            '  <button class="btn btn-danger cancel-board-btn" onclick="Board.cancelEditColumn(this)">' +
            '    Cancel' +
            '  </button>' +
            '</p>');
        $(column).hide();
    };

    /**
     * Removing form for edit column name
     *
     * @param btn
     */
    this.cancelEditColumn = function (btn) {
        $(btn).parent().parent().parent().find('.edit_column').show();
        $('.editing_column').remove();
    };

    /**
     * Sent request for updating chosen column name
     *
     * @param btn
     */
    this.updateColumnName = function (btn) {
        var column_id = $(btn).parent().parent().parent().find('.edit_column').attr('column_id');
        var new_column_name = $('#new_column_name').val().trim();
        if (new_column_name == '') {
            alert('Enter new column name');
        }
        else {
            $.post("/updatecolumnname", {column_id: column_id, new_column_name: new_column_name}, function () {
                Board.updateBoard();
            });
        }
    };

    /**
     * Removes form for edit board name
     * @param btn
     */
    this.cancelEditBoard = function (btn) {
        $(btn).parent().parent().find('.edit_board').show();
        $('.editing_board').remove();
    };

    /**
     * Getting form for edit board name
     *
     * @param board
     */
    this.editBoard = function (board) {
        $(board).parent().append('<p class="editing_board"> ' +
            '  <input id="new_board_name" class="form-control" value="' + $(board).parent().text().trim() + '"/>' +
            '  <button class="btn add-board-btn" onclick="Board.updateBoardName(this)">' +
            '    Update Board' +
            '  </button> ' +
            '  <button class="btn btn-danger cancel-board-btn" onclick="Board.cancelEditBoard(this)">' +
            '    Cancel' +
            '  </button>' +
            '</p>');
        $(board).hide();
    };

    /**
     * Sent request for updating board name
     *
     * @param btn
     */
    this.updateBoardName = function (btn) {
        var board_id = $(btn).parent().parent().find('.edit_board').attr('board_id');
        var new_board_name = $('#new_board_name').val().trim();
        if (new_board_name == "") {
            alert('Set some name to board');
        }
        else {
            $.post("/updateboardname", {board_id: board_id, new_board_name: new_board_name}, function () {
                Board.updateBoard();
            });
        }
    };

    /**
     * Removes form for edit spent time
     */
    this.canceladdSpentTime = function () {
        $('#add_time_btn').show();
        $('#CanceladdSpentTime').remove();
        $('#addspendtime_form').remove();
    };

    /**
     * Getting form for add spent time
     *
     * @param btn
     * @param cart_id
     */
    this.getFormForAddingTime = function (btn, cart_id) {
        $.post("/getformforaddingtime", {cart_id: cart_id}, function (data) {
            $(btn).hide();
            $(btn).before(data);
        });
    };

    /**
     * Sends request for adding new comment in cart
     */
    this.addCommentsinCart = function () {
        var cart_id = $('.cart-info').attr('cart_id');
        var comment = $('#new-comment').val().trim();
        if (comment == '') {
            alert('Enter comment');
        }
        else {
            $.post("/addcommentsincart", {cart_id: cart_id, comment: comment}, function (data) {
                Board.openCart($('.cart-info').attr('cart_id'));
            });
        }
    };

    /**
     * Getting form for new comment
     */
    this.getCommentForm = function () {
        $.post("/getcommentform", function (data) {
            $('#get-comment-form').hide();
            $('#get-comment-form').before(data);
        });
    };

    /**
     * Closing open cart
     */
    this.cloaseCart = function () {
        $('.cart-info').remove();
        $('.black-background').hide();
        $('.carts').hide();
        var board_id = $('.columns').attr('board_id').trim();
        Board.openBoard(board_id);
    };

    /**
     * Removes form for add new comment
     */
    this.cancelNewComment = function () {
        $('.comment-form').remove();
        $('#get-comment-form').show();
    };

    /**
     * Sends request to create new checkout
     */
    this.addCheckout = function () {
        var cart_id = $('.cart-info').attr('cart_id');
        var checkout_name = $('#new-checkout-name').val().trim();
        var comment = $('#new-checkout-comment').val().trim();
        var is_done = $('#is-checkout-done').is(":checked");
        if (checkout_name == '') {
            alert('Enter checkout name');
        }
        else {
            $.post("/addcheckoutincart", {
                cart_id: cart_id,
                checkout_name: checkout_name,
                comment: comment,
                is_done: is_done
            }, function (data) {
                Board.openCart($('.cart-info').attr('cart_id'));
            });
        }
    };

    /**
     * Getting form for adding new checkout
     */
    this.getCheckoutForm = function () {
        $.post("/getcheckoutform", function (data) {
            $('#add_checkout').hide();
            $('#add_checkout').before(data);
        });
    };

    /**
     * Updates cart info
     */
    this.cancelNewCheckout = function () {
        Board.openCart($('.cart-info').attr('cart_id'));
    };

    /**
     * Getting information about chosen checkout
     *
     * @param checklist_id
     * @param btn
     */
    this.openChecklist = function (checklist_id, btn) {
        $.post("/openchecklist", {checklist_id: checklist_id}, function (data) {
            $(btn).hide();
            $('#add_checkout').before(data);
        });
    };

    /**
     * Updates cart info
     */
    this.closeCheckouts = function () {
        Board.openCart($('.cart-info').attr('cart_id'));
    };

    /**
     * Getting form for adding new comment in checkout
     */
    this.getChecklistCommentForm = function () {
        $.post("/getchecklistcommentform", function (data) {
            $('#get-comment-form').hide();
            $('#get-comment-form').before(data);
        });
    };

    /**
     * Sends request for adding new comment in checkout
     * @param btn
     */
    this.addCommentsinChecklist = function (btn) {
        var comment = $('#new-comment').val().trim();
        var checklist_id = $(btn).parent().parent().parent().attr('checklis_id');
        if (comment == '') {
            alert('Write some comments, please');
        }
        else {
            $.post("/addcommentinchecklist", {checklist_id: checklist_id, comment: comment}, function (data) {
                $.post("/openchecklist", {checklist_id: checklist_id}, function (data) {
                    $(btn).parent().parent().parent().remove();
                    $('#add_checkout').before(data);
                });
            });
        }
    };

    /**
     * Removes form for editing new comment in checklist
     * @param btn
     */
    this.cancelNewChecklistComment = function (btn) {
        $(btn).parent().remove();
        $('#get-comment-form').show();
    };

    /**
     * Sends request for setting chosen checkout as done
     * @param btn
     */
    this.setAsDoneChecklist = function (btn) {
        var checklist_id = $(btn).parent().parent().attr('checklis_id');
        $.post("/setasdonedhecklist", {checklist_id: checklist_id}, function (data) {
            Board.openCart($('.cart-info').attr('cart_id'));
        });
    };

    /**
     * Sends request for removing chosen cart
     */
    this.removeCart = function () {
        var cart_id = $('.cart-info').attr('cart_id');
        $.post("/removecart", {cart_id: cart_id}, function (data) {
            var board_id = $('.columns').attr('board_id').trim();
            Board.openBoard(board_id);
            $('.carts').html('');
            $('.carts').hide();
        });
    };

    /**
     * Sends request for removing chosen column
     * @param column_id
     */
    this.removeColumn = function (column_id) {
        $.post("/removecolumn", {column_id: column_id}, function () {
            var board_id = $('.columns').attr('board_id').trim();
            Board.openBoard(board_id);
        });
    };

    /**
     * Sends request for removing chosen board
     * @param board_id
     */
    this.removeBoard = function (board_id) {
        $.post("/removeboard", {board_id: board_id}, function (data) {
            Board.updateBoard();
        });
    };

    /**
     * Getting form for adding new attachments
     */
    this.getAttachmentForm = function () {
        var cart_id = $('.cart-info').attr('cart_id');
        $.post("/getattachmentform", {cart_id: cart_id}, function (data) {
            $('#add_attachment_btn').hide();
            $('#add_attachment_btn').before(data);
        });
    };

    /**
     * Removes form for adding new attachments
     */
    this.cancelUploadFiles = function () {
        $('#add_attachment_btn').show();
        $('.add_attackments_form').remove();
    };

    /**
     * Sends request for getting form of updating estimate time
     */
    this.getFormForUpdateEstimateTime = function () {
        $('.update_estimate_time').remove();
        var cart_id = $('.cart-info').attr('cart_id');
        $.post("/getFormForUpdateEstimateTime", {cart_id: cart_id}, function (data) {
            $('.cart-times').before(data);
        });
    };

    /**
     * Removes form for updating estimate time
     * @param event
     */
    this.cancelUpdateEstimateTime = function (event) {
        event.preventDefault();
        $('.update_estimate_time').remove();
    };

    /**
     * Getting form for updating cart name
     */
    this.getFormForUpdateCartName = function () {
        $('#update_cart_name').remove();
        var cart_id = $('.cart-info').attr('cart_id');
        $('.cart-titel').before('<div id="update_cart_name">' +
            '<input id="updated_cart_name" class="form-control" placeholder="Set new name" />' +
            '<button class="btn btn-success" onclick="Board.updateCartName()">Update</button>' +
            '<button class="btn btn-warning" onclick="Board.cancelUpdateCartName()" >Cancel</button>' +
            '</div>');
    };

    /**
     * Send request form updating cart name
     */
    this.updateCartName = function () {
        var cart_id = $('.cart-info').attr('cart_id');
        var cart_name = $('#updated_cart_name').val();
        if (cart_name == '') {
            alert('Set some name');
        }
        else {
            $.post("/UpdateCartName", {cart_id: cart_id, cart_name: cart_name}, function (data) {
                Board.openCart(cart_id);
            });
        }
    };

    /**
     * Removes form for editing cart name
     */
    this.cancelUpdateCartName = function () {
        $('#update_cart_name').remove();
    };

    /**
     * Getting form for updaing cart description
     */
    this.getFormForUpdateCartDescription = function () {
        $('#update_cart_description').remove();
        var cart_id = $('.cart-info').attr('cart_id');
        $('.description').before('<div id="update_cart_description">' +
            '<textarea id="cart_description" class="form-control" placeholder="Write new description"></textarea>' +
            '<button class="btn btn-success" onclick="Board.updateCartDescription()">Update</button>' +
            '<button class="btn btn-warning" onclick="Board.cancelUpdateCartDescription()">Cancel</button>' +
            '</div>');
    };

    /**
     * Sends request for updating cart description
     */
    this.updateCartDescription = function () {
        var cart_id = $('.cart-info').attr('cart_id');
        var cart_description = $('#cart_description').val().trim();
        if (cart_description == '') {
            alert('Set some description');
        }
        else {
            $.post("/UpdateCartDescription", {cart_id: cart_id, cart_description: cart_description}, function (data) {
                Board.openCart(cart_id);
            });
        }
    };

    /**
     * Removes form for updating cart description
     */
    this.cancelUpdateCartDescription = function () {
        $('#update_cart_description').remove();
    };

    /**
     * Sends request for getting form for updating cart members
     */
    this.getFormForUpdateCartMembers = function () {
        $('#update_cart_members').remove();
        var cart_id = $('.cart-info').attr('cart_id');
        $.post("/getFormForUpdateCartMembers", {cart_id: cart_id}, function (data) {
            $('.members').before(data);
        });
    };

    /**
     * Removes form for updating cart members
     *
     * @param event
     */
    this.cancelUpdateCartMembers = function (event) {
        event.preventDefault();
        $('#update_cart_members').remove();
    };
    /**
     * Getting columns from chosen board
     *
     * @param board_id
     */
    this.openBoard = function (board_id) {
        $.post("/getColumnsByBoardID", {board_id: board_id}, function (data) {
            $('.boards').html('');
            $('.columns-page').remove();
            $('.black-background').hide();
            $('#fountainG').hide();
            $('.boards').before(data);
            $('body').addClass('board_opened');
        });
    };
};
