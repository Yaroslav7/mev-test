<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Cart;
use AppBundle\Interfaces\DraggableModelInterface;
use AppBundle\Interfaces\TableModelInterface;
use AppBundle\Traits\TableTrait;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * CartRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 *
 * CartRepository implements all methods which have to be for work with information about carts in database
 *
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
class CartRepository extends \Doctrine\ORM\EntityRepository implements TableModelInterface, DraggableModelInterface
{
    /**
     * @see TableTrait
     */
    use TableTrait;

    /**
     * Getting information about all carts.
     *
     * @see TableModelInterface
     *
     * @return array
     */
    public function get()
    {
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('SELECT *
             FROM cart             
             ORDER BY cart.cart_number');
        $statement->execute();
        $results = $statement->fetchAll();

        return $results;
    }

    /**
     * Adding new record of a cart in database.
     *
     * @see TableModelInterface
     *
     * @param $user_id
     * @param $data
     *
     * @return int
     */
    public function add($user_id, $data)
    {
        $cart = new Cart();
        $em = $this->getEntityManager();
        $cart->setName($data['cart_name']);
        $user_id = $em->getReference('AppBundle\Entity\User', $user_id);
        $column_id = $em->getReference('AppBundle\Entity\Columns', $data['column_id']);
        $cart->setUserId($user_id);
        $cart->setAttachments($data['files_path']);
        $cart->setColumnId($column_id);
        $cart->setCartNumber($this->getLastListNumber() + 1);
        $cart->setCreatedTime(new \DateTime());
        $cart->setMembers(serialize($data['cart_members']));
        $cart->setDescription($data['cart_description']);
        $estimate_time = $this->convertTimeInMins($data['estimate_months'], $data['estimate_days'], $data['estimate_hours'], $data['estimate_mins']);
        $cart->setEstimatedTime($estimate_time);
        $cart->setSpendTime(0);
        $em->persist($cart);
        $em->flush();

        return $cart->getId();
    }

    /**
     * Getting max serial(position) number of carts.
     *
     * @see TableModelInterface
     *
     * @return int
     */
    public function getLastListNumber()
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT MAX(p.cart_number)
            FROM AppBundle:Cart p'
        );
        if (isset($query->getResult()[0][1])) {
            return $query->getResult()[0][1];
        } else {
            return 0;
        }
    }

    /**
     * Getting chosen cart information.
     *
     * @see TableModelInterface
     *
     * @param $cart_id
     *
     * @return array
     */
    public function getCartInfo($cart_id)
    {
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('SELECT *
             FROM cart             
             WHERE cart.id = :id');
        $statement->bindParam(':id', $cart_id, \PDO::PARAM_INT);
        $statement->execute();
        $results = $statement->fetchAll();

        return $results;
    }

    /**
     * Adding spent time to chosen cart.
     *
     * @see TableModelInterface
     *
     * @param $spend_time
     * @param $cart_id
     *
     * @return mixed
     */
    public function addSpendTime($spend_time, $cart_id)
    {
        $em = $this->getEntityManager();
        $cart = $em->getRepository('AppBundle:Cart')->find($cart_id);
        if (!$cart) {
            throw new Exception(
                'No cart found for id '.$cart_id
            );
        }
        $cart->setSpendTime($spend_time + $cart->getSpendTime());
        $em->flush();

        return $cart->getId();
    }

    /**
     * Removing chosen cart.
     *
     * @see TableModelInterface
     *
     * @param int $id
     *
     * @return bool
     */
    public function remove(int $id)
    {
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('DELETE FROM `cart`                    
             WHERE cart.id = :id');
        $statement->bindParam(':id', $id, \PDO::PARAM_INT);

        return $statement->execute();
    }

    /**
     * Getting carts which owned by chosen column.
     *
     * @param $column_id
     *
     * @return array
     */
    public function getCartsByColumn($column_id)
    {
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('Select * 
             FROM `cart`                    
             WHERE cart.column_id = :id
             ORDER BY cart.cart_number');
        $statement->bindParam(':id', $column_id, \PDO::PARAM_INT);
        $statement->execute();
        $results = $statement->fetchAll();

        return $results;
    }

    /**
     * Changing chosen cart's column.
     *
     * @param $cart_id
     * @param $new_column_id
     *
     * @return mixed
     */
    public function changeCartColumn($cart_id, $new_column_id)
    {
        $em = $this->getEntityManager();
        $new_column_id = $em->getReference('AppBundle\Entity\Columns', $new_column_id);
        $cart = $em->getRepository('AppBundle:Cart')->find($cart_id);

        if (!$cart) {
            throw new Exception(
                'No cart found for id '.$cart_id
            );
        }
        $cart->setColumnId($new_column_id);
        $em->flush();

        return $cart->getId();
    }

    /**
     * Set chosen cart as last in chosen column.
     *
     * @param $cart_id
     * @param $new_column_id
     * @param $prev_num
     *
     * @return mixed
     */
    public function setAsLast($cart_id, $new_column_id, $prev_num)
    {
        $this->ChangeCartColumn($cart_id, $new_column_id);
        $prev_num = $prev_num + 1;
        $em = $this->getEntityManager();

        $cart = $em->getRepository('AppBundle:Cart')->find($cart_id);
        if (!$cart) {
            throw new Exception(
                'No cart found for id '.$cart_id
            );
        }
        $cart->setCartNumber($prev_num);
        $em->flush();

        return $cart->getId();
    }

    /**
     * Set chosen cart as first in chosen column.
     *
     * @param $cart_id
     * @param $new_column_id
     * @param $next_num
     *
     * @return array
     */
    public function setAsFirst($cart_id, $next_num, $new_column_id)
    {
        $this->ChangeCartColumn($cart_id, $new_column_id);
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('SELECT cart.id
              FROM cart 
              WHERE cart.column_id = :new_column_id AND cart.id != :id AND cart.cart_number >= :next_num 
              ORDER BY cart.cart_number');
        $statement->bindParam(':new_column_id', $new_column_id, \PDO::PARAM_INT);
        $statement->bindParam(':id', $cart_id, \PDO::PARAM_INT);
        $statement->bindParam(':next_num', $next_num, \PDO::PARAM_INT);
        $statement->execute();
        $results = $statement->fetchAll();
        $this->UpdateNumberById($cart_id, $next_num);
        $cart_number = $next_num + 1;
        foreach ($results as $result) {
            $this->UpdateNumberById($result['id'], $cart_number);
            ++$cart_number;
        }

        return $results;
    }

    /**
     * Updating cart serial(position) number in column.
     *
     * @param $cart_id
     * @param $new_number
     *
     * @return mixed
     */
    public function UpdateNumberById($cart_id, $new_number)
    {
        $em = $this->getEntityManager();
        $cart = $em->getRepository('AppBundle:Cart')->find($cart_id);
        if (!$cart) {
            throw new Exception(
                'No cart found for id '.$cart_id
            );
        }
        $cart->setCartNumber($new_number);
        $em->flush();

        return $cart->getId();
    }

    /**
     * @param $cart_id
     * @param $new_column_id
     * @param $prev_num
     * @param $next_num
     *
     * @return bool
     */
    public function setBetweenElements($cart_id, $new_column_id, $prev_num, $next_num)
    {
        $set_as_last = $this->SetAsLast($cart_id, $new_column_id, $prev_num);
        $result = ($prev_num + 1) == $next_num;
        if ($result) {
            $set_as_first = $this->SetAsFirst($cart_id, $next_num, $new_column_id);

            return $set_as_first && $set_as_last;
        }

        return $set_as_last xor $result;
    }

    /**
     * Updating atachments in chosen cart.
     *
     * @param $cart_id
     * @param $attachments
     *
     * @return int
     */
    public function updateAttachments($cart_id, $attachments)
    {
        $em = $this->getEntityManager();
        $cart = $em->getRepository('AppBundle:Cart')->find($cart_id);
        if (!$cart) {
            throw new Exception(
                'No cart found for id '.$cart_id
            );
        }
        $cart->setAttachments($attachments);
        $em->flush();

        return $cart->getId();
    }

    /**
     * Updating members in chosen cart.
     *
     * @param $cart_id
     * @param $members
     *
     * @return int
     */
    public function updateCartMembers($cart_id, $members)
    {
        $em = $this->getEntityManager();
        $cart = $em->getRepository('AppBundle:Cart')->find($cart_id);
        if (!$cart) {
            throw new Exception(
                'No cart found for id '.$cart_id
            );
        }
        $cart->setMembers($members);
        $em->flush();

        return $cart->getId();
    }

    /**
     * Updating description in chosen cart.
     *
     * @param $cart_id
     * @param $cart_description
     *
     * @return int
     */
    public function updateCartDescription($cart_id, $cart_description)
    {
        $em = $this->getEntityManager();
        $cart = $em->getRepository('AppBundle:Cart')->find($cart_id);
        if (!$cart) {
            throw new Exception(
                'No cart found for id '.$cart_id
            );
        }
        $cart->setDescription($cart_description);
        $em->flush();

        return $cart->getId();
    }

    /**
     * Updating estimate time in a chosen cart.
     *
     * @param $estimate_time
     * @param $cart_id
     *
     * @return int
     */
    public function updateEstimateTime($estimate_time, $cart_id)
    {
        $em = $this->getEntityManager();
        $cart = $em->getRepository('AppBundle:Cart')->find($cart_id);
        if (!$cart) {
            throw new Exception(
                'No cart found for id '.$cart_id
            );
        }
        $cart->setEstimatedTime($estimate_time);
        $em->flush();

        return $cart->getId();
    }

    /**
     * Updating name in a chosen cart.
     *
     * @see TableModelInterface
     *
     * @param int    $cart_id
     * @param string $cart_name
     *
     * @return mixed
     */
    public function update(int $cart_id, string $cart_name)
    {
        $em = $this->getEntityManager();
        $cart = $em->getRepository('AppBundle:Cart')->find($cart_id);
        if (!$cart) {
            throw new Exception(
                'No cart found for id '.$cart_id
            );
        }
        $cart->setName($cart_name);
        $em->flush();

        return $cart->getId();
    }
}
