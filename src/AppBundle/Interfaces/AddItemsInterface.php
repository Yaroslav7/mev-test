<?php

namespace AppBundle\Interfaces;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface AddItemsInterface.
 *
 * TableInterface is interface that give ability to add new boards, columns, carts and checkouts
 *
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
interface AddItemsInterface
{
    /**
     * addAction method says that we have to implemented method of creating of new specimen.
     *
     * If it'll be implement in Board class it have to create a new board.
     * If it'll be implement in Column class it have to create a new column.
     * If it'll be implement in Cart class it have to create a new cart.
     * If it'll be implement in Checklist class it have to create a new checkout.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function addAction(Request $request);
}
