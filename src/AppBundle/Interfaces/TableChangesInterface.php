<?php

namespace AppBundle\Interfaces;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface TableChangesInterface.
 *
 * TableInterface is interface that helps to determine some special methods of Kanban Board classes
 *
 * User will have ability to remove, update boards, columns, carts
 *
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
interface TableChangesInterface
{
    /**
     * updateAction method says that we have to implement an updating method.
     *
     * If it'll be implement in Board class it have to update chosen board.
     * If it'll be implement in Column class it have to update chosen column.
     * If it'll be implement in Cart class it have to update chosen cart.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function updateAction(Request $request);

    /**
     * removeAction method says that we have to implement an removing method.
     *
     * If it'll be implement in Board class it have to remove chosen board.
     * If it'll be implement in Column class it have to remove chosen column.
     * If it'll be implement in Cart class it have to remove chosen cart.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function removeAction(Request $request);
}
