<?php

namespace AppBundle\Interfaces;

/**
 * Interface BoardInfoInterface.
 *
 * BoardInfoInterface is interface that give ability  ability to get information about all boards and getting form
 * for creation of new boards
 *
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
interface BoardInfoInterface
{
    /**
     * getInfoAction method says that we have to implement method of getting information.
     *
     * It'll be implement in Board class it have to give information about chosen board.
     *
     * @return mixed
     */
    public function getInfoAction();

    /**
     * getFormAction method says that we have to implement method of getting new form.
     *
     * It'll be implement in Board class it have to give form for creation of new board.
     *
     * @return mixed
     */
    public function getFormAction();
}
