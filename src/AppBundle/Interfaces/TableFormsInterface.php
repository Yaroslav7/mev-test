<?php

namespace AppBundle\Interfaces;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface TableFormsInterface.
 *
 * InfoInterface is interface that give ability to get forms for new columns, carts and checkouts
 *
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
interface TableFormsInterface
{
    /**
     * getFormAction method says that we have to implement method of getting new form.
     *
     * If it'll be implement in Column class it have to give form for creation of new column.
     * If it'll be implement in Cart class it have to give form for creation of new cart.
     * If it'll be implement in Checklist class it have to give form for creation of new checkout.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function getFormAction(Request $request);
}
