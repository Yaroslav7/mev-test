<?php

namespace AppBundle\Interfaces;
/**
 * Interface DraggableInterface
 *
 * DraggableInterface is interface that give ability to implement some functions for changing posiotion
 * numbers of elements id database
 *
 * @package AppBundle\Interfaces
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
interface DraggableModelInterface{

    /**
     * UpdateNumberById will use for updating serial(position) number
     *
     * @param $id
     * @param $number
     * @return mixed
     */
    public function UpdateNumberById($id, $number);

    /**
     * setAsFirst will use for setting item as first in list
     *
     * @param $id
     * @param $parent_id
     * @param $next_num
     * @return mixed
     */
    public function setAsFirst($id, $parent_id, $next_num);

    /**
     * setBetweenElements will use for setting item between two anothers
     *
     * @param $id
     * @param $parent_id
     * @param $prev_num
     * @param $next_num
     * @return mixed
     */
    public function setBetweenElements($id, $parent_id, $prev_num, $next_num);
}