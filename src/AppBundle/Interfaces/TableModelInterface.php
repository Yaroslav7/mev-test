<?php

namespace AppBundle\Interfaces;

/**
 * Interface TableModelInterface.
 *
 * TableModelInterface is interface that helps to determine custom methods for board, column and cart entities
 *
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
interface TableModelInterface
{
    /**
     * Getting information about table.
     *
     * If it'll be board class, it return information about all boards
     * If it'll be column class, it return information about all columns
     * If it'll be cart class, it return information about all carts
     *
     * @return mixed
     */
    public function get();

    /**
     * Updating chosen record in database.
     *
     * If it'll be board class, it update information about all board name
     * If it'll be column class, it update information about all column name
     * If it'll be cart class, it update information about all cart name
     *
     * @param int    $id
     * @param string $name
     *
     * @return int
     */
    public function update(int $id, string $name);

    /**
     * Adding new record in database.
     *
     * If it'll be board class, it add new board
     * If it'll be column class, it add new column
     * If it'll be cart class, it add new cart
     *
     * @param $id
     * @param $name
     *
     * @return int
     */
    public function add($id, $name);

    /**
     * Remove chosen item from database.
     *
     * If it'll be board class, it remove chosen board
     * If it'll be column class, it remove chosen column
     * If it'll be cart class, it remove chosen cart
     *
     * @param int $id
     *
     * @return bool
     */
    public function remove(int $id);

    /**
     * Getting last serial number of record(max position number).
     *
     * If it'll be board class, it return information about last serial(position) number of boards
     * If it'll be column class, it return information about last serial(position) number of columns
     * If it'll be cart class, it return information about last serial(position) number of carts
     *
     * @return int
     */
    public function getLastListNumber();
}
