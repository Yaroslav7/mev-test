<?php

namespace AppBundle\Interfaces;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface InfoInterface.
 *
 * InfoInterface is interface that give ability to get information about carts and checkouts
 *
 * @package AppBundle\Interfaces
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
interface InfoInterface
{
    /**
     * getInfoAction method says that we have to implement method of getting information.
     *
     * If it'll be implement in Cart class it have to give information about chosen cart.
     * If it'll be implement in Checklist class it have to give information about chosen checkout.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function getInfoAction(Request $request);
}
