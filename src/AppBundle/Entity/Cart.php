<?php

// src/AppBundle/Entity/Cart.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CartRepository")
 * @ORM\Table(name="cart")
 */
class Cart
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToMany(targetEntity="Checklist", mappedBy="cart_id")
     * @ORM\OneToMany(targetEntity="Comments", mappedBy="cart_id")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Columns", inversedBy="id")
     * @ORM\JoinColumn(name="column_id", referencedColumnName="id")
     */
    private $column_id;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="id")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $cart_number;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created_time;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $estimated_time;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $spend_time;

    /**
     * @ORM\Column(type="text")
     */
    private $attachments;

    /**
     * @ORM\Column(type="text")
     */
    private $members;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Cart
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Cart
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return Cart
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set createdTime.
     *
     * @param \DateTime $createdTime
     *
     * @return Cart
     */
    public function setCreatedTime($createdTime)
    {
        $this->created_time = $createdTime;

        return $this;
    }

    /**
     * Get createdTime.
     *
     * @return \DateTime
     */
    public function getCreatedTime()
    {
        return $this->created_time;
    }

    /**
     * Set estimatedTime.
     *
     * @param int $estimatedTime
     *
     * @return Cart
     */
    public function setEstimatedTime($estimatedTime)
    {
        $this->estimated_time = $estimatedTime;

        return $this;
    }

    /**
     * Get estimatedTime.
     *
     * @return int
     */
    public function getEstimatedTime()
    {
        return $this->estimated_time;
    }

    /**
     * Set spendTime.
     *
     * @param int $spendTime
     *
     * @return Cart
     */
    public function setSpendTime($spendTime)
    {
        $this->spend_time = $spendTime;

        return $this;
    }

    /**
     * Get spendTime.
     *
     * @return int
     */
    public function getSpendTime()
    {
        return $this->spend_time;
    }

    /**
     * Set attachments.
     *
     * @param string $attachments
     *
     * @return Cart
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments.
     *
     * @return string
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Set members.
     *
     * @param string $members
     *
     * @return Cart
     */
    public function setMembers($members)
    {
        $this->members = $members;

        return $this;
    }

    /**
     * Get members.
     *
     * @return string
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Set columnId.
     *
     * @param int $columnId
     *
     * @return Cart
     */
    public function setColumnId($columnId)
    {
        $this->column_id = $columnId;

        return $this;
    }

    /**
     * Get columnId.
     *
     * @return int
     */
    public function getColumnId()
    {
        return $this->column_id;
    }

    /**
     * Set cartNumber.
     *
     * @param int $cartNumber
     *
     * @return Cart
     */
    public function setCartNumber($cartNumber)
    {
        $this->cart_number = $cartNumber;

        return $this;
    }

    /**
     * Get cartNumber.
     *
     * @return int
     */
    public function getCartNumber()
    {
        return $this->cart_number;
    }
}
