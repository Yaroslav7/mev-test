<?php

// src/AppBundle/Entity/Product.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentsRepository")
 * @ORM\Table(name="comments")
 */
class Comments
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="id")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cart", inversedBy="id")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id")
     */
    private $cart_id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Checklist", inversedBy="id")
     * @ORM\JoinColumn(name="checklist_id", referencedColumnName="id")
     */
    private $checklist_id;

    /**
     * @ORM\Column(type="text")
     */
    private $comment;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment.
     *
     * @param string $comment
     *
     * @return Comments
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return Comments
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set cartId.
     *
     * @param int $cartId
     *
     * @return Comments
     */
    public function setCartId($cartId = null)
    {
        $this->cart_id = $cartId;

        return $this;
    }

    /**
     * Get cartId.
     *
     * @return int
     */
    public function getCartId()
    {
        return $this->cart_id;
    }

    /**
     * Set checklistId.
     *
     * @param int $checklistId
     *
     * @return Comments
     */
    public function setChecklistId($checklistId = null)
    {
        $this->checklist_id = $checklistId;

        return $this;
    }

    /**
     * Get checklistId.
     *
     * @return int
     */
    public function getChecklistId()
    {
        return $this->checklist_id;
    }
}
