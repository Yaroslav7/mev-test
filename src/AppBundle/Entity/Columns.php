<?php
// src/AppBundle/Entity/Columns.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ColumnsRepository")
 * @ORM\Table(name="columns")
 */
class Columns
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToMany(targetEntity="Cart", mappedBy="column_id")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $list_number;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Board", inversedBy="id")
     * @ORM\JoinColumn(name="board_number", referencedColumnName="id")
     */
    private $board_number;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Columns
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set listNumber.
     *
     * @param int $listNumber
     *
     * @return Columns
     */
    public function setListNumber($listNumber)
    {
        $this->list_number = $listNumber;

        return $this;
    }

    /**
     * Get listNumber.
     *
     * @return int
     */
    public function getListNumber()
    {
        return $this->list_number;
    }

    /**
     * Set boardNumber.
     *
     * @param int $boardNumber
     *
     * @return Columns
     */
    public function setBoardNumber($boardNumber)
    {
        $this->board_number = $boardNumber;

        return $this;
    }

    /**
     * Get boardNumber.
     *
     * @return int
     */
    public function getBoardNumber()
    {
        return $this->board_number;
    }
}
