<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DefaultRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToMany(targetEntity="Board", mappedBy="user_id")
     * @ORM\OneToMany(targetEntity="Column", mappedBy="user_id")
     * @ORM\OneToMany(targetEntity="Cart", mappedBy="user_id")
     * @ORM\OneToMany(targetEntity="Comments", mappedBy="user_id")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
}
