<?php

namespace AppBundle\Controller;

use AppBundle\Interfaces\AddItemsInterface;
use AppBundle\Interfaces\TableChangesInterface;
use AppBundle\Interfaces\TableFormsInterface;
use AppBundle\Traits\TableTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ColumnsController.
 *
 * It provides methods to common features needed in columns.
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
class ColumnsController extends Controller implements AddItemsInterface, TableChangesInterface, TableFormsInterface
{
    /**
     * @see TableTrait
     */
    use TableTrait;

    /**
     * addAction is methods that adds a new column in a chosen board.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addAction(Request $request)
    {
        $board_id = $request->request->get('board_id');
        $name = $request->request->get('name');

        $em = $this->getDoctrine()->getManager();
        $add_new_column = $em->getRepository('AppBundle:Columns')
            ->add($board_id, $name);

        return new Response($add_new_column);
    }

    /**
     * getFormAction is method that get form for creating a new column in choosen board.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getFormAction(Request $request)
    {
        $board_id = $request->request->get('board_id');

        return $this->render('AppBundle:Columns:add_new_colum.html.twig', array('board_id' => $board_id));
    }

    /**
     * updateAction is method that updates chosen column name.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateAction(Request $request)
    {
        $column_id = $request->request->get('column_id');
        $new_column_name = $request->request->get('new_column_name');
        $em = $this->getDoctrine()->getManager();
        $update = $em->getRepository('AppBundle:Columns')
            ->update($column_id, $new_column_name);

        return new Response($update);
    }

    /**
     * removeAction is method that removes chosen column.
     *
     * At first it finds all carts, comments and checkoits in this column. Then remove all comments from carts and checkouts, after it
     * method will remove all cart in this board. And after all it will remove chosen column.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function removeAction(Request $request)
    {
        $column_id = $request->request->get('column_id');
        $em = $this->getDoctrine()->getManager();
        $carts = $em->getRepository('AppBundle:Cart')
            ->getCartsByColumn($column_id);
        foreach ($carts as $cart) {
            $cart_exist = $em->getRepository('AppBundle:Cart')
                ->getCartInfo($cart['id']);
            if (!count($cart_exist) <= 0) {
                $em->getRepository('AppBundle:Comments')
                    ->removeCommentsByCartId($cart['id']);

                $em->getRepository('AppBundle:Checklist')
                    ->removeChecklistByCartId($cart['id']);

                $em->getRepository('AppBundle:Cart')
                    ->remove($cart['id']);
            }
        }
        $column_id = $em->getRepository('AppBundle:Columns')
            ->remove($column_id);

        return new Response('removed column with id - ' . $column_id);
    }

    /**
     * Getting info about columns and cart in chosen board
     *
     * @param Request $request
     * @return Response
     */
    public function getColumnsByBoardIDAction(Request $request)
    {
        $board_id = $request->request->get('board_id');
        $em = $this->getDoctrine()->getManager();
        $board = $em->getRepository('AppBundle:Board')
            ->getInfoByBoardId($board_id);
        $columns = $em->getRepository('AppBundle:Columns')
            ->getByBoard($board_id);
        $carts = array();
        if (!count($columns) == 0) {
            foreach ($columns as $column) {
                $cart = $em->getRepository('AppBundle:Cart')
                    ->getCartsByColumn($column['id']);
                $carts = array_merge($carts, $cart);
            }
        }
        $user = $this->getUser();
        $user_id = $this->getUserId($user);

        return $this->render('AppBundle:Columns:updated_columns.html.twig', array('board' => $board, 'columns' => $columns, 'carts' => $carts, 'user_id' => $user_id));
    }

    /**
     * Updating columns positions
     *
     * @param Request $request
     * @return Response
     */
    public function updateColumnPositionAction(Request $request)
    {
        $data = $request->request->get('data');
        if (!isset($data['column_id'])) {
            throw new Exception('Cannot get column_id');
        }
        $em = $this->getDoctrine()->getManager();
        if (!isset($data['prev_el']) && !isset($data['next_el'])) {
            return new Response('Element stay on his previous position');
        } else {
            if (!isset($data['next_el'])) {
                $em->getRepository('AppBundle:Columns')
                    ->setAsLast($data['column_id'], $data['prev_el']);
            } else {
                if (!isset($data['prev_el'])) {
                    $em->getRepository('AppBundle:Columns')
                        ->setAsFirst($data['column_id'],  $data['board_id'], $data['next_el']);
                } else {
                    $em->getRepository('AppBundle:Columns')
                        ->setBetweenElements($data['column_id'], $data['board_id'], $data['prev_el'], $data['next_el']);
                }
            }
            return new Response('Column position changed');
        }
    }

}
