<?php

namespace AppBundle\Controller;

use AppBundle\Interfaces\AddItemsInterface;
use AppBundle\Interfaces\BoardInfoInterface;
use AppBundle\Interfaces\TableChangesInterface;
use AppBundle\Traits\TableTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BoardController.
 *
 * It provides methods to common features needed in boards.
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
class BoardController extends Controller implements AddItemsInterface, TableChangesInterface, BoardInfoInterface
{
    /**
     * @see TableTrait;
     */
    use TableTrait;

    /**
     * Get information about each board, columns on board and carts in columns.
     *
     * @return Response
     *
     * @see BoardInfoInterface
     */
    public function getInfoAction()
    {
        $em = $this->getDoctrine()->getManager();
        $boards = $em->getRepository('AppBundle:Board')
            ->get();
        $columns = $em->getRepository('AppBundle:Columns')
            ->get();
        $carts = $em->getRepository('AppBundle:Cart')
            ->get();
        $user = $this->getUser();
        $user_id = $this->getUserId($user);

        return $this->render('AppBundle:Boards:Updated_Boards.html.twig', array('columns' => $columns, 'carts' => $carts, 'boards' => $boards, 'user_id' => $user_id));
    }

    /**
     * Update chosen board name.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @see TableChangesInterface
     */
    public function updateAction(Request $request)
    {
        $board_id = $request->request->get('board_id');
        $new_board_name = $request->request->get('new_board_name');
        $em = $this->getDoctrine()->getManager();
        $update = $em->getRepository('AppBundle:Board')
            ->update($board_id, $new_board_name);

        return new Response($update);
    }

    /**
     * Getting form for creating of new board.
     *
     * @return Response
     *
     * @see BoardInfoInterface
     */
    public function getFormAction()
    {
        return $this->render('AppBundle:Boards:new_board_form.html.twig');
    }

    /**
     * Getting button which can call method of creation of new board.
     *
     * @return Response
     */
    public function cancelNewBoardFormAction()
    {
        return $this->render('AppBundle:Boards:add_new_board_btn.html.twig');
    }

    /**
     * Creating of new board method.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @see AddItemsInterface\
     */
    public function addAction(Request $request)
    {
        $name = $request->request->get('data');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $user_id = $this->getUserId($user);
        $add_new_board = $em->getRepository('AppBundle:Board')
            ->add($user_id, $name);

        return new Response($add_new_board);
    }

    /**
     * removeAction removing chosen board, all columns, cartsm checkouts and comments on this board.
     *
     * At first it finds all columns, carts, comments and checkoits in this board. Then remove all comments from carts and checkouts, after it
     * method will remove all cart, them column in this board. And after all it will remove chosen board.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @see TableChangesInterface
     */
    public function removeAction(Request $request)
    {
        $board_id = $request->request->get('board_id');
        $em = $this->getDoctrine()->getManager();
        $columns = $em->getRepository('AppBundle:Columns')
            ->getColumnsByBoardId($board_id);
        foreach ($columns as $column) {
            $carts = $em->getRepository('AppBundle:Cart')
                ->getCartsByColumn($column['id']);
            foreach ($carts as $cart) {
                $cart_exist = $em->getRepository('AppBundle:Cart')
                    ->getCartInfo($cart['id']);
                if (!count($cart_exist) <= 0) {
                    $em->getRepository('AppBundle:Comments')
                        ->removeCommentsByCartId($cart['id']);

                    $em->getRepository('AppBundle:checklist')
                        ->removeChecklistByCartId($cart['id']);

                    $em->getRepository('AppBundle:Cart')
                        ->remove($cart['id']);
                }
            }
            $em->getRepository('AppBundle:Columns')
                ->remove($column['id']);
        }
        $board_id = $em->getRepository('AppBundle:Board')
            ->remove($board_id);

        return new Response('removed board with id -'.$board_id);
    }
}
