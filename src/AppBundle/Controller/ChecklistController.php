<?php

namespace AppBundle\Controller;

use AppBundle\Interfaces\InfoInterface;
use AppBundle\Interfaces\AddItemsInterface;
use AppBundle\Interfaces\TableFormsInterface;
use AppBundle\Traits\TableTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ChecklistController.
 *
 * It provides methods to common features needed in checklist.
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
class ChecklistController extends Controller implements AddItemsInterface, InfoInterface, TableFormsInterface
{
    /**
     * @see TableTrait
     */
    use TableTrait;

    /**
     * Adding new checkout to cart.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');
        $checkout_name = $request->request->get('checkout_name');
        $is_done = $request->request->get('is_done');
        $comment = $request->request->get('comment');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $user_id = $this->getUserId($user);
        if ($is_done == 'true') {
            $is_done = true;
        } else {
            $is_done = false;
        }
        $checkout = $em->getRepository('AppBundle:Checklist')
            ->add($cart_id, $is_done, $checkout_name);
        if ($comment != '') {
            $em->getRepository('AppBundle:Comments')
                ->addNewComment($comment, $user_id, null, $checkout);
        }

        return new Response($checkout);
    }

    /**
     * Getting form for adding new checkout.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getFormAction(Request $request)
    {
        return $this->render('AppBundle:Checklist:new_checkout_form.html.twig');
    }

    /**
     * Getting information about chosen checkout.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getInfoAction(Request $request)
    {
        $checklist_id = $request->request->get('checklist_id');
        $em = $this->getDoctrine()->getManager();
        $checklist = $em->getRepository('AppBundle:Checklist')
            ->getChecklist($checklist_id)[0];
        $comments = $em->getRepository('AppBundle:Comments')
            ->getComentInChecklist($checklist_id);

        return $this->render('AppBundle:Checklist:checkout_info.html.twig', array('checklist' => $checklist, 'comments' => $comments));
    }

    /**
     * Getting form for adding comments in checkout.
     *
     * @return Response
     */
    public function getChecklistCommentFormAction()
    {
        return $this->render('AppBundle:Checklist:checklist_comment_form.html.twig');
    }

    /**
     * Set chosen checkout as done.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function setAsDoneChecklistAction(Request $request)
    {
        $checklist_id = $request->request->get('checklist_id');
        $em = $this->getDoctrine()->getManager();
        $checklist = $em->getRepository('AppBundle:Checklist')
            ->setChecklistAsDone($checklist_id);

        return new Response($checklist);
    }

    /**
     * Adding comment into chosen checkout.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addCommentInChecklistAction(Request $request)
    {
        $checklist_id = $request->request->get('checklist_id');
        $comment = $request->request->get('comment');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $user_id = $this->getUserId($user);
        $add_comment = $em->getRepository('AppBundle:Comments')
            ->addNewComment($comment, $user_id, null, $checklist_id);

        return new Response($add_comment);
    }
}
