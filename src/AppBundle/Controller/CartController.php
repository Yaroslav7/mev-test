<?php

namespace AppBundle\Controller;

use AppBundle\Interfaces\AddItemsInterface;
use AppBundle\Interfaces\InfoInterface;
use AppBundle\Interfaces\TableChangesInterface;
use AppBundle\Interfaces\TableFormsInterface;
use AppBundle\Traits\TableTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CartController.
 *
 * It provides methods to common features needed in cart.
 * In CartController implemented methods of creating new carts, updating chosen carts, removing chosen carts, changing positions of chosen carts, getting form for creation
 * new carts, attachments and comments, getiing information about chosen cart.
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
class CartController extends Controller implements AddItemsInterface, TableChangesInterface, InfoInterface, TableFormsInterface
{
    /**
     * @see TableTrait
     */
    use TableTrait;

    /**
     * addAction will adding a new cart in column.
     *
     * If user attached some files they will be same in upload file folder. The path of upload file folder can be find in config.yml
     * by parameter - upload_directory. In database will be save a path to file but not a file. Array members in cart will be serrialized and saved in database.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addAction(Request $request)
    {
        /*
         * Getting params
         */
        $data = $request->request->all();
        $files = $request->files->get('files');
        /*
         * Saving files
         */
        $max_size = $this->getServerMaxFileUpload();
        $files_path = array();
        if (!is_null($files)) {
            foreach ($files as $file) {
                if (isset($file)) {
                    if ($file->getClientSize() < $max_size && $file->getClientSize() != 0) {
                        $file->move($this->getParameter('upload_directory'), $file->getClientOriginalName());
                        array_push($files_path, $this->getParameter('upload_directory').$file->getClientOriginalName());
                    } else {
                        return new Response('File size is bigger then server allows or file is brocken');
                    }
                }
            }
        }
        /*
         * Insert values
         */
        $files_path = serialize($files_path);
        $data['files_path'] = $files_path;
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $user_id = $this->getUserId($user);
        $add_new_cart = $em->getRepository('AppBundle:Cart')
            ->add($user_id, $data);
        if (!empty($data['cart_comment'])) {
            $em->getRepository('AppBundle:Comments')
                ->addNewComment($data['cart_comment'], $user_id, $add_new_cart, null);
        }

        return new Response($add_new_cart);
    }

    /**
     * Getting form for creating a new cart.
     * Using column_id for inserting cart feature in right column(in which this cart was created).
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getFormAction(Request $request)
    {
        $column_id = $request->request->get('column_id');
        if (is_null($column_id)) {
            throw new Exception('cannot get column id');
        }
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:User')
            ->getUsers();

        return $this->render('AppBundle:Carts:Cart_form.html.twig', array('users' => $users, 'column_id' => $column_id));
    }

    /**
     * Getting information about chosen cart.
     *
     * In getInfoAction method will get chosen cart id. Then check if such cart exist in database. If current cart exist then will get comments which wrote in cart,
     * getting checklist of cart, unserialized member of cart and getting their names by their id, getting a list of attachments. By existing checkouts will calculate a cart
     * progress.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getInfoAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');
        if (is_null($cart_id)) {
            throw new Exception('cannot get cart id');
        }
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('AppBundle:Cart')
            ->getCartInfo($cart_id);
        if (count($cart) <= 0) {
            return new Response('<p>Cannot find a cart</p> <div class="cart-options"> <button class="btn btn-warning" onclick="Board.CloaseCart()"> Close Cart </button> </div>');
        }
        $comments = $em->getRepository('AppBundle:Comments')
            ->getComentInCart($cart_id);
        $checklist = $em->getRepository('AppBundle:checklist')
            ->getChecklists($cart_id);
        $members = unserialize($cart[0]['members']);
        if (count($members) == 0) {
            return new Response('false');
        }
        $members_names = array();
        foreach ($members as $member) {
            array_push($members_names, $em->getRepository('AppBundle:User')
                ->getUsernameById($member)[0]['username']);
        }
        $attachments = unserialize($cart[0]['attachments']);
        $new_att = array();
        foreach ($attachments as $attachment) {
            $arr = (['name' => explode($this->getParameter('upload_directory'), $attachment)[1], 'path' => $attachment]);
            array_push($new_att, $arr);
        }
        $user = $this->getUser();
        $user_id = $this->getUserId($user);
        $progress = count($checklist);
        if ($progress != 0) {
            $iterator = 0;
            foreach ($checklist as $item) {
                if ($item['is_done']) {
                    ++$iterator;
                }
            }
            $progress = 100 * $iterator / $progress.'%';
        } else {
            $progress = 'No chekouts to count the progress';
        }

        return $this->render('AppBundle:Carts:Cart_info.html.twig', array('progress' => $progress, 'checklist' => $checklist, 'attachments' => $new_att, 'cart' => $cart[0], 'comments' => $comments, 'members' => $members_names, 'user_id' => $user_id));
    }

    /**
     * getFormForAddingTimeAction is getting form for adding spent time to cart.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getFormForAddingTimeAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('AppBundle:Cart')
            ->getCartInfo($cart_id);
        if (count($cart) <= 0) {
            return new Response('<p>Cannot find a cart</p>');
        }

        return $this->render('AppBundle:Carts:adding_time_form.html.twig', array('cart_id' => $cart_id));
    }

    /**
     * addSpendTimeAction is adding a spent time to cart.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addSpendTimeAction(Request $request)
    {
        $data = $request->request->all();
        $spend_time = $this->convertTimeInMins($data['estimate_months'], $data['estimate_days'], $data['estimate_hours'], $data['estimate_mins']);
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('AppBundle:Cart')
            ->addSpendTime($spend_time, $data['cart_id']);
        $response = new Response();
        $response->setContent(json_encode(array('data' => $cart)));
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * getCommentFormAction is getting form for adding new comment.
     *
     * @return Response
     */
    public function getCommentFormAction()
    {
        return $this->render('AppBundle:Carts:Comment_form.html.twig');
    }

    /**
     * addCommentsInCartAction is adding new comment in cart.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addCommentsInCartAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');
        $comment = $request->request->get('comment');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $user_id = $this->getUserId($user);
        $cart = $em->getRepository('AppBundle:Comments')
            ->addNewComment($comment, $user_id, $cart_id, null);

        return new Response($cart);
    }

    /**
     * removeAction is removing a chosen cart with comments and checkouts in it.
     *
     * At first it finds all comments and checkoits in this cart. Then remove all comments from carts and checkouts. And after all it will remove chosen cart.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function removeAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('AppBundle:Cart')
            ->getCartInfo($cart_id);
        if (count($cart) <= 0) {
            return new Response('<p>Cannot find a cart</p>');
        }
        $em->getRepository('AppBundle:Comments')
            ->removeCommentsByCartId($cart_id);
        $em->getRepository('AppBundle:checklist')
            ->removeChecklistByCartId($cart_id);
        $em->getRepository('AppBundle:Cart')
            ->remove($cart_id);

        return new Response('removed');
    }

    /**
     * updateCartPositionAction.
     *
     * If column doesn't have any cart, so we can add this cart to current column and her serial number doesn't matter.
     * If cart was added to the end of the column list, then her serial number will be change to last_el+1.
     * If cart was added at the beginning of column, then will give this car serial number - 1 and other elements serial number will be updated by order of their
     * previous serial numbers.
     * If cart was add at the middle of list. Will get previous element serial number. Set current cart serial number as serial number of prev_el+1. And for next element
     * if serial number is last_el+1, then will add +1 for all others serial numbers of next elements. If next_el serial number is bigger then last_el+1, so nothing to do.
     *
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateCartPositionAction(Request $request)
    {
        $data = $request->request->get('data');
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('AppBundle:Cart')
            ->getCartInfo($data['cart_id']);
        if (count($cart) <= 0) {
            return new Response('<p>Cannot find a cart</p>');
        }
        if (!isset($data['next_el']) && !isset($data['prev_el'])) { //no elements in column
            $em->getRepository('AppBundle:Cart')
                ->changeCartColumn($data['cart_id'], $data['new_column']);
        } else {
            if (!isset($data['next_el'])) {   // don't have next elements, he is last in column
                $em->getRepository('AppBundle:Cart')
                    ->setAsLast($data['cart_id'], $data['new_column'], $data['prev_el']);
            }
            if (!isset($data['prev_el'])) { // don't have prev elements, he is first in column
                $em->getRepository('AppBundle:Cart')
                    ->setAsFirst($data['cart_id'], $data['next_el'], $data['new_column']);
            }
            if (isset($data['next_el']) && isset($data['prev_el'])) { //between two elements in column
                $em->getRepository('AppBundle:Cart')
                    ->setBetweenElements($data['cart_id'], $data['new_column'], $data['prev_el'], $data['next_el']);
            }
        }

        return new Response('Position changed');
    }

    /**
     * getAttachmentFormAction is method that gives form for adding new attachments to carts.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getAttachmentFormAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');

        return $this->render('AppBundle:Carts:add_attachment_form.html.twig', array('cart_id' => $cart_id));
    }

    /**
     * addAttachmentAction is method that adding attachment to chosen cart.
     *
     * At first it will save file in directory for uploaded files. Then it will get array of attachments of this cart and add new path(s) of added attachment(s).
     * After it update record with attachment info in database for current cart.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addAttachmentAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');
        $files = $request->files->get('files');
        if (is_null($files)) {
            throw new Exception('no files posted');
        }
        $max_size = $this->getServerMaxFileUpload();
        $files_path = array();
        foreach ($files as $file) {
            if (isset($file)) {
                if ($file->getClientSize() < $max_size && $file->getClientSize() != 0) {
                    $file->move($this->getParameter('upload_directory'), $file->getClientOriginalName());
                    array_push($files_path, $this->getParameter('upload_directory').$file->getClientOriginalName());
                } else {
                    return new Response('File size is bigger then server allows or file is brocken');
                }
            } else {
                return new Response('Cannot find any file');
            }
        }
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('AppBundle:Cart')
            ->getCartInfo($cart_id);
        $attachments = unserialize($cart[0]['attachments']);
        $attachments = array_merge($attachments, $files_path);
        $attachments = serialize($attachments);
        $em->getRepository('AppBundle:Cart')
            ->updateAttachments($cart_id, $attachments);

        return new Response('uploaded');
    }

    /**
     * Getting max size of files which can be upload on server.
     *
     * @return int|string
     */
    protected function getServerMaxFileUpload()
    {
        $max_size = trim(ini_get('upload_max_filesize'));
        $last = strtolower($max_size[strlen($max_size) - 1]);
        switch ($last) {
            case 'g':
                $max_size *= 1024;
            case 'm':
                $max_size *= 1024;
            case 'k':
                $max_size *= 1024;
        }

        return $max_size;
    }

    /**
     * Getting form for updaing estimate time.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getFormForUpdateEstimateTimeAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');

        return $this->render('AppBundle:Carts:UpdateEstimateForm.html.twig', array('cart_id' => $cart_id));
    }

    /**
     * Getting form for updating members of cart.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getFormForUpdateCartMembersAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:User')
            ->getUsers();

        return $this->render('AppBundle:Carts:UpdateMembersForm.html.twig', array('cart_id' => $cart_id, 'users' => $users));
    }

    /**
     * updateAction is method that updating cart name.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');
        $cart_name = $request->request->get('cart_name');
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('AppBundle:Cart')
            ->update($cart_id, $cart_name);

        return new Response($cart);
    }

    /**
     * updateEstimateTimeAction is method that updating estimate time.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateEstimateTimeAction(Request $request)
    {
        $data = $request->request->all();
        $estimate_time = $this->convertTimeInMins($data['update_estimate_months'], $data['update_estimate_days'], $data['update_estimate_hours'], $data['update_estimate_mins']);
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('AppBundle:Cart')
            ->updateEstimateTime($estimate_time, $data['cart_id']);

        return new Response($cart);
    }

    /**
     * updateCartDescriptionAction is method that updating cart description.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateCartDescriptionAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');
        $cart_description = $request->request->get('cart_description');
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('AppBundle:Cart')
            ->updateCartDescription($cart_id, $cart_description);

        return new Response($cart);
    }

    /**
     * updateCartMembersAction is method that updating cart members.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateCartMembersAction(Request $request)
    {
        $cart_id = $request->request->get('cart_id');
        $members = $request->request->get('cart_members');
        $members = serialize($members);
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('AppBundle:Cart')
            ->updateCartMembers($cart_id, $members);

        return new Response($cart);
    }
}
