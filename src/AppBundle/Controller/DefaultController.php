<?php

namespace AppBundle\Controller;

use AppBundle\Traits\TableTrait;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController.
 *
 * Default controller for rendering home page.
 * Also in this controller realized google account authorization.
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
class DefaultController extends Controller
{
    /**
     * @see TableTrait
     */
    use TableTrait;

    /**
     * Getting home page.
     *
     * @Route("/", name="homepage")
     *
     * @return Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $boards = $em->getRepository('AppBundle:Board')
            ->get();
        $columns = $em->getRepository('AppBundle:Columns')
            ->get();
        $carts = $em->getRepository('AppBundle:Cart')
            ->get();
        $user = $this->getUser();
        $user_id = $this->getUserId($user);

        return $this->render('AppBundle:Default:index.html.twig', array('columns' => $columns, 'boards' => $boards, 'user_id' => $user_id, 'carts' => $carts));
    }

    /**
     * Getting login form.
     *
     * @Route("/login-form", name="login_form")
     *
     * @return Response $response
     */
    public function login_form()
    {
        header('Access-Control-Allow-Origin: *');

        $request = Request::createFromGlobals();

        $response = new Response();
        $response->setContent('Log in or create a new account if you don\'t have it yet
            <a class="btn btn-lg btn-primary btn-block" href="/login">Login</a>
            <a class="btn btn-lg btn-primary btn-block" href="/register">Register</a>
            <div id="gSignIn"></div>');
        $request->request->get('data');
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'text/html');

        return $response;
    }

    /**
     * If such user is not registered yet, then register him in database without password.
     * If user already registered, then create a new session for him and login him.
     *
     * @Route("/login-google", name="login_google")
     *
     * @return Response $response
     */
    public function login_google()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $data = $request->request->get('data');
        $response = new Response();
        if ($data == 'false') {
            if (session_status() != 2) {
                $response->setContent(json_encode(array('data' => 'false')));
            } else {
                if (!(empty($_SESSION['email']) && empty($_SESSION['name']))) {
                    $response->setContent(json_encode(array('data' => 'true')));
                }
            }
        } else {
            if (session_status() != 2) {
                session_start();
            }
            $em = $this->getDoctrine()->getManager();
            $registered = $em->getRepository('AppBundle:User')
                ->checkExustingUser($data['email']);

            $data['registered'] = $registered;
            if ($registered) {
                $em->getRepository('AppBundle:User')
                    ->registerGoogleUser($data);
            }
            $_SESSION['name'] = $data['name'];
            $_SESSION['email'] = $data['email'];
            $response->setContent(json_encode(array('data' => $data)));
        }
        $response->setContent(json_encode(array('data' => $data)));
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Destroying session for current user.
     *
     * @Route("/logout-google", name="logout_google")
     *
     * @return Response $response
     */
    public function logout_google()
    {
        $response = new Response();
        $response->setContent(json_encode(array('data' => session_destroy())));
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
