<?php

namespace AppBundle\Traits;

use AppBundle\Entity\User;

/**
 * Trait TableTrait.
 *
 * In this trait implemented common methods that will be user in all classes of kanbab board realization.
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
trait TableTrait
{
    /**
     * convertTimeInMins method convert time in minutes for saving estimate and spent time in database in minutest format. It made for easier calculation of time.
     *
     * @param int $months
     * @param int $days
     * @param int $hours
     * @param int $mins
     *
     * @return int
     */
    public function convertTimeInMins($months, $days, $hours, $mins)
    {
        return $months * 43200 + $days * 1440 + $hours * 60 + $mins;
    }

    /**
     * getUserId method gives id of current user.
     *
     * @param $user
     *
     * @return mixed
     */
    public function getUserId($user)
    {
        $em = $this->get('doctrine')->getManager();
        if (is_null($user)) {
            $user_id = $em->getRepository('AppBundle:User')
                ->getUserId();
            if (!$user_id) {
                $user_id = 'Cannot find user';
            }
        } else {
            $user_id = $user->getId();
        }

        return $user_id;
    }
}
