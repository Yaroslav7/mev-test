<?php

namespace Tests\AppBundle\Repository;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BoardRepositoryTest extends KernelTestCase
{
    /**
     * Data provider for testUpdate.
     *
     * @return array
     */
    public function getDataForUpdate()
    {
        return [
            [3, 'board name'],
            [4, 'new name'],
            [78, 'Board name'],
        ];
    }

    /**
     * Testing parameters in update function.
     *
     * @dataProvider getDataForUpdate
     *
     * @param $id
     * @param $name
     */
    public function testUpdate($id, $name)
    {
        $classMock = $this->getBoardRepositoryrMock();
        $classMock
            ->method('update')
            ->with(
                $this->isType('int'),
                $this->isType('string')
            );
        $classMock->update($id, $name);
    }

    /**
     * Get BoardRepository mock.
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getBoardRepositoryrMock()
    {
        $mock = $this->getMockBuilder(self::class)
            ->setMethods(['update', 'add'])
            ->disableOriginalConstructor()
            ->getMock();

        return $mock;
    }
}
