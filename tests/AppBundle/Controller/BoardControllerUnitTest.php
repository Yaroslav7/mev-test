<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Controller\BoardController;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BoardControllerUnitTest extends KernelTestCase
{
    /**
     * Test addAction that works with set parameters.
     */
    public function testAddAction()
    {
        $mock = $this->getMockBuilder(BoardController::class)
            ->disableOriginalConstructor()
            ->getMock();
        $request = \Symfony\Component\HttpFoundation\Request::create('/addboard', 'POST', array());
        $mock->expects($this->once())
            ->method('addAction')
            ->with($request);
        $mock->addAction($request);
    }

    /**
     * Test updateAction that works with set parameters.
     */
    public function testUpdateAction()
    {
        $classMock = $this->getBoardControllerMock();
        $request = \Symfony\Component\HttpFoundation\Request::create('/updateboardname', 'POST', array());
        $classMock->expects($this->once())
            ->method('updateAction')
            ->with($this->equalTo($request));
        $classMock->updateAction($request);
    }

    /**
     * Get BoardController mock.
     *
     * @return BoardController|\PHPUnit_Framework_MockObject_MockObject
     */
    protected function getBoardControllerMock()
    {
        $mock = $this->getMockBuilder(BoardController::class)
            ->setMethods(['updateAction', 'addAction'])
            ->disableOriginalConstructor()
            ->getMock();

        return $mock;
    }
}
