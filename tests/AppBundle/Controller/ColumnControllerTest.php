<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ColumnControllerTest.
 *
 * In ColumnControllerTest implemented functional tests for ColumnController
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
class ColumnControllerTest extends WebTestCase
{
    /**
     * Testing column form for existing add and cancel buttons.
     */
    public function testGetNewColumnForm()
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/getnewcolumnform');

        $this->assertCount(2, $crawler->filter('.btn'));
        $this->assertContains('Create Column', $crawler
            ->filter('.btn')
            ->first()
            ->text()
        );
        $this->assertContains('Cancel', $crawler
            ->filter('.btn')
            ->last()
            ->text()
        );
    }
}
