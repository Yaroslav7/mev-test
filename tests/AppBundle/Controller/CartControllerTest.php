<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class CartControllerTest.
 *
 * In CartControllerTest implemented functional tests for CartController
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
class CartControllerTest extends WebTestCase
{
    /**
     * Tests for existing text of close button in new cart form.
     */
    public function testGetNewCartForm()
    {
        $client = static::createClient();
        $crawler = $client->request(
            'POST',
            '/getcartinfo',
            array('cart_id' => '1')
        );
        /*
         * Check is there is close btn.
         */
        $this->assertContains('Close Cart', $crawler
            ->filter('.cart-options button')
            ->first()
            ->text()
        );
    }
}
