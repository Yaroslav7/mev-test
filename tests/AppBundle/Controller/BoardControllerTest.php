<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class BoardControllerTest.
 *
 * In BoardControllerTest implemented functional tests for BoardController
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
class BoardControllerTest extends WebTestCase
{
    /**
     * Testing board form for existing create and cancel buttons.
     */
    public function testGetNewBoardForm()
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/getnewboardform');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertCount(2, $crawler->filter('.btn'));
        $this->assertContains('Create Board', $crawler
            ->filter('.btn')
            ->first()
            ->text()
        );
        $this->assertContains('Cancel', $crawler
            ->filter('.btn')
            ->last()
            ->text()
        );
    }

    /**
     * Testing cancel new form for existing only 1 button on it.
     */
    public function testCancelNewBoardForm()
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/cancelnewboardform');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('button'));
    }

    /**
     * Testing updating of board.
     */
    public function testUpdate()
    {
        $client = static::createClient();
        $res = $client->request('POST', '/updateboardname', (array('board_id' => '3', 'new_board_name' => 'TestDesc')))->text();
        $this->assertEquals(
            200, $client->getResponse()->getStatusCode()
        );
        $this->assertEquals(
            3, $res
        );
    }

}
