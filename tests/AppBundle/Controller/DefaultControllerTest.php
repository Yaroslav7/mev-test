<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest.
 *
 * In DefaultControllerTest implemented functional tests for DefaultController
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
class DefaultControllerTest extends WebTestCase
{
    /**
     * Testing home page for right header text.
     */
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Kanban Board', $crawler
            ->filter('.header-title')
            ->text()
        );
    }
}
