<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ChecklistControllerTest.
 *
 * In ChecklistControllerTest implemented functional tests for ChecklistController
 *
 * @author Yaroslav Solodovnikov <solodovnikov.yar@gmail.com>
 *
 * @version 1.0
 */
class ChecklistControllerTest extends WebTestCase
{
    /**
     * Testing checkout form for existing cancel and add buttons.
     */
    public function testGetCheckoutForm()
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/getcheckoutform');

        $this->assertCount(2, $crawler->filter('.btn'));
        $this->assertContains('Add', $crawler
            ->filter('.btn')
            ->first()
            ->text()
        );
        $this->assertContains('Cancel', $crawler
            ->filter('.btn')
            ->last()
            ->text()
        );
    }
}
