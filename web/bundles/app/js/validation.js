var Validate = new function () {
    /**
     * Validate datatime
     *
     * @param months
     * @param days
     * @param hours
     * @param mins
     * @returns {*}
     */
    this.validateTime = function (months, days, hours, mins) {
        if (months < 0 || days < 0 || hours < 0 || mins < 0) {
            return 'time caanot be less then 0';
        }
        if (months == '') {
            return 'estimate month';
        }
        else {
            if (months > 12) {
                return 'months right, it cannot be more them 12 months';
            }
            else {
                if (days == '') {
                    return 'estimate days';
                }
                else {
                    if (days > 30) {
                        return 'days right, it cannot be more them 30 days';
                    }
                    else {
                        if (hours == '') {
                            return 'estimate hours';
                        }
                        else {
                            if (hours > 24) {
                                return 'hours right, it cannot be more them 24 hours';
                            }
                            else {
                                if (mins == '') {
                                    return 'estimate mins';
                                }
                                else {
                                    if (mins > 60) {
                                        return 'mins right, it cannot be more them 60 mins';
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    /**
     * Validate new cart info
     *
     * @param cart_info
     * @returns {*}
     */
    this.validateNewCart = function validateCartForm(cart_info) {
        var column_id = $(cart_info).find('#column_id').val().trim();
        var cart_name = $(cart_info).find('#cart_name').val().trim();
        var cart_description = $(cart_info).find('#cart_description').val().trim();
        var cart_members = $(cart_info).find('#cart_members').val();
        var estimate_months = $(cart_info).find('#estimate_months').val().trim();
        var estimate_days = $(cart_info).find('#estimate_days').val().trim();
        var estimate_hours = $(cart_info).find('#estimate_hours').val().trim();
        var estimate_mins = $(cart_info).find('#estimate_mins').val().trim();
        if (column_id == '') {
            return 'column id';
        }
        else {
            if (cart_name == '') {
                return 'cart name';
            }
            else {
                if (cart_description == '') {
                    return 'cart description';
                }
                else {
                    if (cart_members == null) {
                        return 'cart member';
                    }
                    else {
                        return Validate.validateTime(estimate_months, estimate_days, estimate_hours, estimate_mins);
                    }

                }
            }
        }
    }

};